﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DB_Viewer.Model;
using System.Data.Entity.Core.Objects;
using System.Data.Entity;
using System.Data;
using CefSharp;
using CefSharp.Wpf;
using System.Diagnostics;

namespace DB_Viewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DB_Viewer.WarshipsDataSet warshipsDataSet;
        DB_Viewer.WarshipsDataSetTableAdapters.WarshipTableAdapter warshipsDataSetWarshipTableAdapter;
        System.Windows.Data.CollectionViewSource warshipViewSource;
        System.Windows.Data.CollectionViewSource comparisonViewSource;
        WarshipsEntities warshipsEntities;
        BindingListCollectionView view;

        public MainWindow() => InitializeComponent();

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            warshipsDataSet = ((DB_Viewer.WarshipsDataSet)(this.FindResource("warshipsDataSet")));
            // Load data into the table Warship. You can modify this code as needed.
            warshipsDataSetWarshipTableAdapter = new DB_Viewer.WarshipsDataSetTableAdapters.WarshipTableAdapter();
            warshipsDataSetWarshipTableAdapter.Fill(warshipsDataSet.Warship);
            warshipViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("warshipViewSource")));
            comparisonViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("comparisonViewSource")));
            warshipViewSource.View.MoveCurrentToFirst();
            comparisonViewSource.View.MoveCurrentToLast();
            warshipsEntities = new WarshipsEntities();
            Search_Grid.ItemsSource = warshipsDataSet.Warship;
            view = (BindingListCollectionView)CollectionViewSource.GetDefaultView(Search_Grid.ItemsSource);
            Comparison();
        }

        private void Forward_Click(object sender, RoutedEventArgs e)
        {
            if (warshipViewSource.View.CurrentPosition == (warshipViewSource.View as BindingListCollectionView).Count - 1)
            {
                warshipViewSource.View.MoveCurrentToFirst();
            }
            else
            {
                warshipViewSource.View.MoveCurrentToNext();
            }
            Comparison();

        }

        private void Backward_Click(object sender, RoutedEventArgs e)
        {
            if (warshipViewSource.View.CurrentPosition == 0)
            {
                warshipViewSource.View.MoveCurrentToLast();
            } else
            {
                warshipViewSource.View.MoveCurrentToPrevious();
            }
            Comparison();

        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            if (warshipsDataSet.HasChanges())
            {
                try
                {                  
                    warshipsDataSetWarshipTableAdapter.Connection.Open();
                    warshipsDataSetWarshipTableAdapter.Update(warshipsDataSet);
                    warshipsDataSetWarshipTableAdapter.Connection.Close();
                }
                catch
                {
                    MessageBox.Show("Update failed");
                }
            }
            else
            {
                MessageBox.Show("No changes made");
            }
        }

        private void Search(object sender, RoutedEventArgs e)
        {
            string Filter = "";
            Filter = SetupSearch();
            if (Search_Box.Text != "")
            {
                if (Filter == "")
                {
                    Filter = " Name LIKE '%";
                }
                else
                { 
                    Filter += "AND Name LIKE '%";
                }
                Filter += Search_Box.Text.ToString();
                Filter += "%'";
            }
                try
                {
                    view.CustomFilter = Filter;
                }
                catch
                {
                    MessageBox.Show(Filter + " not found");
                }
        }

        private void To_Overview(object sender, RoutedEventArgs e)
        {
            var WarshipID = (((Button)sender).Tag).ToString();
            warshipViewSource.View.MoveCurrentToPosition(Convert.ToInt32(WarshipID)-1);
            MainControl.SelectedIndex = 0;
            Comparison();
        }

        private void To_Compare(object sender, RoutedEventArgs e)
        {
            var WarshipID = (((Button)sender).Tag).ToString();
            comparisonViewSource.View.MoveCurrentToPosition(Convert.ToInt32(WarshipID) - 1);
            MainControl.SelectedIndex = 2;
            Comparison();
        }

        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }

        private void Backward_Comp_Click(object sender, RoutedEventArgs e)
        {
            if (comparisonViewSource.View.CurrentPosition == 0)
            {
                comparisonViewSource.View.MoveCurrentToLast();
            }
            else
            {
                comparisonViewSource.View.MoveCurrentToPrevious();
            }
            Comparison();
        }

        private void Forward_Comp_Click(object sender, RoutedEventArgs e)
        {
            if (comparisonViewSource.View.CurrentPosition == (comparisonViewSource.View as BindingListCollectionView).Count - 1)
            {
                comparisonViewSource.View.MoveCurrentToFirst();
            }
            else
            {
                comparisonViewSource.View.MoveCurrentToNext();
            }
            Comparison();
        }
        
        private void Comparison()
        {
            int ship1 = 0;
            int ship2 = 0;
            BitmapImage leftArrow = new BitmapImage(new Uri("pack://siteoforigin:,,,/Images/Icons/leftarrow.png"));
            BitmapImage rightArrow = new BitmapImage(new Uri("pack://siteoforigin:,,,/Images/Icons/rightarrow.png"));
            BitmapImage line = new BitmapImage(new Uri("pack://siteoforigin:,,,/Images/Icons/line.png"));

            if (Convert.ToDouble(Ship1Speed.Content) > Convert.ToDouble(Ship2Speed.Content))
            {
                Speed_Comparison.Source = leftArrow;               
                ship1++;
            }
            else if (Convert.ToDouble(Ship1Speed.Content) < Convert.ToDouble(Ship2Speed.Content))
            {
                Speed_Comparison.Source = rightArrow;
                ship2++;
            }
            else
            {
                Speed_Comparison.Source = line;
            }

            if (Convert.ToDouble(Ship1GunRange.Content) > Convert.ToDouble(Ship2GunRange.Content))
            {
                Gun_Comparison.Source = leftArrow;
                ship1++;
            }
            else if(Convert.ToDouble(Ship1GunRange.Content) < Convert.ToDouble(Ship2GunRange.Content))
            {
                Gun_Comparison.Source = rightArrow;
                ship2++;
            }
            else
            {
                Gun_Comparison.Source = line;
            }

            if (Convert.ToDouble(Ship1TorpRange.Content) > Convert.ToDouble(Ship2TorpRange.Content))
            {
                Torp_Comparison.Source = leftArrow;
                ship1++;
            }
            else if (Convert.ToDouble(Ship1TorpRange.Content) < Convert.ToDouble(Ship2TorpRange.Content))
            {
                Torp_Comparison.Source = rightArrow;
                ship2++;
            }
            else
            {
                Torp_Comparison.Source = line;
            }

            if (Convert.ToDouble(Ship1Concealment.Content) < Convert.ToDouble(Ship2Concealment.Content))
            {
                Concealment_Comparison.Source = leftArrow;
                ship1++;
            }
            else if (Convert.ToDouble(Ship1Concealment.Content) > Convert.ToDouble(Ship2Concealment.Content))
            {
                Concealment_Comparison.Source = rightArrow;
                ship2++;
            }
            else
            {
                Concealment_Comparison.Source = line;
            }            

            if(ship1 > ship2)
            {
                Winner.Content = "Winner " + ship1 + " : " + ship2;
            }
            else if( ship2 > ship1)
            {
                Winner.Content = ship1 + " : " + ship2 + " Winner";
            }
            else
            {
                Winner.Content = ship1 + " : " + ship2;
            }
        }

        //The 115 lines that shall not be mentioned
        private string SetupSearch()
        {
            string Filter="";
            string Nation="";
            string Class="";
            string Tier="";
            if(AircraftCarrier.IsChecked==true)
            {
                Class += "OR Type ='Images\\Icons\\Class\\carrier.png' ";
            }
            if (Battleship.IsChecked == true)
            {
                Class += "OR Type ='Images\\Icons\\Class\\battleship.png' ";
            }
            if (Cruiser.IsChecked == true)
            {
                Class += "OR Type ='Images\\Icons\\Class\\cruiser.png' ";
            }
            if (Destroyer.IsChecked == true)
            {
                Class += "OR Type ='Images\\Icons\\Class\\destroyer.png' ";
            }
            if(I.IsChecked == true)
            {
                Tier += "OR Tier ='I' ";
            }
            if (II.IsChecked == true)
            {
                Tier += "OR Tier = 'II' ";
            }
            if (III.IsChecked == true)
            {
                Tier += "OR Tier = 'III' ";
            }
            if (IV.IsChecked == true)
            {
                Tier += "OR Tier = 'IV' ";
            }
            if (V.IsChecked == true)
            {
                Tier += "OR Tier = 'V' ";
            }
            if (VI.IsChecked == true)
            {
                Tier += "OR Tier = 'VI' ";
            }
            if (VII.IsChecked == true)
            {
                Tier += "OR Tier = 'VII' ";
            }
            if (VIII.IsChecked == true)
            {
                Tier += "OR Tier = 'VIII' ";
            }
            if (IX.IsChecked == true)
            {
                Tier += "OR Tier = 'IX' ";
            }
            if (X.IsChecked == true)
            {
                Tier += "OR Tier = 'X' ";
            }
            if(Commonwealth.IsChecked==true)
            {
                Nation += "OR Nation = 'Images\\Icons\\Nations\\commonwealth.png' ";
            }
            if (France.IsChecked == true)
            {
                Nation += "OR Nation = 'Images\\Icons\\Nations\\france.png' ";
            }
            if (Germany.IsChecked == true)
            {
                Nation += "OR Nation = 'Images\\Icons\\Nations\\germany.png' ";
            }
            if (Italy.IsChecked == true)
            {
                Nation += "OR Nation = 'Images\\Icons\\Nations\\italy.png' ";
            }
            if (Japan.IsChecked == true)
            {
                Nation += "OR Nation = 'Images\\Icons\\Nations\\japan.png' ";
            }
            if (Panamerica.IsChecked == true)
            {
                Nation += "OR Nation = 'Images\\Icons\\Nations\\pan-america.png' ";
            }
            if (Panasia.IsChecked == true)
            {
                Nation += "OR Nation = 'Images\\Icons\\Nations\\pan-asia.png' ";
            }
            if (Poland.IsChecked == true)
            {
                Nation += "OR Nation = 'Images\\Icons\\Nations\\poland.png' ";
            }
            if (Uk.IsChecked == true)
            {
                Nation += "OR Nation = 'Images\\Icons\\Nations\\uk.png' ";
            }
            if (Usa.IsChecked == true)
            {
                Nation += "OR Nation = 'Images\\Icons\\Nations\\usa.png' ";
            }
            if (Ussr.IsChecked == true)
            {
                Nation += "OR Nation = 'Images\\Icons\\Nations\\ussr.png' ";
            }

            if (Nation != "")
            {
                Nation = "AND " + Nation.Substring(2);
            }
            if (Class != "")
            {
                Class = "AND " + Class.Substring(2);
            }
            if (Tier != "")
            {
                Tier = "AND " + Tier.Substring(2);
            }

            Filter += Nation;
            Filter += Class;
            Filter += Tier;

            if (Filter != "")
            {
                Filter = Filter.Substring(3);
            }

            return Filter;
        }
    }
}
